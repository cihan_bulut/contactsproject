package com.cb.codecs;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;

import com.cb.model.Contact;

public class ContactCodec implements Codec<Contact> {
	private final CodecRegistry codecRegistry;
	public ContactCodec(final CodecRegistry codecRegistry) {
		this.codecRegistry = codecRegistry;
	}
	public void encode(BsonWriter writer, Contact t, EncoderContext arg2) {
		Document document = new Document();
		document.append("name", t.getName());
		document.append("lastName", t.getLastName());
		document.append("phone", t.getPhone());
		writer.writeString(document.toJson());
		
	}

	public Class<Contact> getEncoderClass() {
		// TODO Auto-generated method stub
		return Contact.class;
	}

	public Contact decode(BsonReader reader, DecoderContext dc) {
		String json = reader.readString();
		return new Contact(Document.parse(json));
	}

}
