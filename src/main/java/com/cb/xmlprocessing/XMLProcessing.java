package com.cb.xmlprocessing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.cb.model.Contact;


public class XMLProcessing {
	public static Document parseXMLFile(File file) throws DocumentException {
		SAXReader reader = new SAXReader();
		Document document = (Document)reader.read(file);
		return document;
	}
	public static void writeDataFile(List<Contact> mergedContactList,File file){
		StringBuilder b = new StringBuilder();
	for (Contact contact : mergedContactList) {
		b.append("{\n");System.out.println("{");
		b.append(" name : " + contact.getName()+"\n");System.out.println(" name : " + contact.getName());
		b.append(" lastName : " + contact.getLastName()+"\n");System.out.println(" lastName : " + contact.getLastName());
		b.append(" phone : [ "+"\n");System.out.println(" phone : [ ");
		for (String phoneC : contact.getPhone()) {
			b.append(phoneC +" , "+"\n");System.out.println(phoneC +" , ");
		}
		b.append(" ] "+ "\n");System.out.println(" ] ");
		b.append("}"+ "\n");System.out.println("}");
	}
	File file2 = file;
	FileWriter outputStream = null;
	
	try {
			if(!file2.exists()){
				file2.createNewFile();
				outputStream = new FileWriter(file2);
				outputStream.write(b.toString());
				outputStream.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
