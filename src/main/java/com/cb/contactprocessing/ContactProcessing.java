package com.cb.contactprocessing;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.cb.model.Contact;
import com.cb.xmlprocessing.XMLProcessing;

public class ContactProcessing {
	public List<Contact> getContactsList(File file){
    	Document document = null;
    	Element rootElement= null;
    	Iterator<Node> iterator = null;
    	List<Node> name = null;
    	List<Contact> contacts = new ArrayList<Contact>();
    	try{
    	document = XMLProcessing.parseXMLFile(file);
    	name = document.getRootElement().selectNodes("/contacts/contact");
    	Set<String> phone = null;
     	for(Node node : name){
     		phone = new HashSet<String>();
     		phone.add(node.selectSingleNode("phone").getText());
    		contacts.add(new Contact(node.selectSingleNode("name").getText(),node.selectSingleNode("lastName").getText(),phone));
    	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return contacts;
	}
	public List<Contact> mergeDuplicateRecords(List<Contact> contacts){
		Map<String,Set<String>> nameAndLastName = new HashMap<String, Set<String>>();
    	Iterator<Contact> iterator = contacts.iterator();
    	Set<String> phone = null;
    	while (iterator.hasNext()) {
			Contact contact = (Contact) iterator.next();
			if(nameAndLastName.containsKey(contact.getName() + "_" + contact.getLastName())){
				for (String ph : contact.getPhone()) {
					nameAndLastName.get(contact.getName() + "_" + contact.getLastName()).add(ph);
				}
			}else{
				phone = new HashSet<String>();
				for (String string : contact.getPhone()) {
					phone.add(string);
				}
				nameAndLastName.put(contact.getName() + "_" + contact.getLastName(),phone);
			}
		}
    	Iterator<String> iteratorMap = nameAndLastName.keySet().iterator();
    	List<Contact> newContactList = new ArrayList<Contact>();
    	while (iteratorMap.hasNext()) {
			String string = (String) iteratorMap.next();
			String[] nameAndLastNameString = string.split("_");
			newContactList.add(new Contact(nameAndLastNameString[0], nameAndLastNameString[1], nameAndLastName.get(string)));
		}
		return newContactList;
	}
}
