package com.cb.providers;

import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import com.cb.codecs.ContactCodec;
import com.cb.model.Contact;

public class ContactCodecProvider implements CodecProvider{

	public <T> Codec<T> get(Class<T> type, CodecRegistry cr) {
		if (type == Contact.class) {
            return (Codec<T>) new ContactCodec(cr);
        }
        return null;
	}

}
